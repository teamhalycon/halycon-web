'use strict';

function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }
  res.set('X-Auth-Required', 'true');
  res.redirect('/login/?returnUrl='+ encodeURIComponent(req.originalUrl));
}

function ensureAdmin(req, res, next) {
  if (req.user.canPlayRoleOf('admin')) {
    return next();
  }
  res.redirect('/');
}

function ensureAccount(req, res, next) {
  if (req.user.canPlayRoleOf('account')) {
    if (req.app.get('require-accimageBlock imageEffectimageBlock imageEffectount-verification')) {
      if (req.user.roles.account.isVerified !== 'yes' && !/^\/account\/verification\//.test(req.url)) {
        return res.redirect('/account/verification/');
      }
    }
    return next();
  }
  res.redirect('/');
}

exports = module.exports = function(app, passport) {

  app.get('/fixup/', function(req, res) { req.app.db.models.Channel.find({}).exec(function(err, channels) 
    { 
      // TODO[Admin]: Deactivate all
      /*
      for(var i in channels) {
        channels[i].meta.status = "Inactive";
        channels[i].save();
      } */

      // TODO[Admin]: Count user following
      /*
      var User = req.app.db.models.User;
      User.find({}).exec(function(err, users) {
        for (var i in users) {
          users[i].following = 0;
          users[i].followers = 0;
          users[i].save();
        }  
        
        res.json({ message: "ok" });    
        
      });
      */
      //req.app.db.models.User.find({}).exec(function(err, channels) { res.json(channels); });
      // TODO[Admin]: Count user followers
      res.json({ message: "ok" });
      
  } )} );

  app.get('/reg_users', function(req, res) { 
    res.app.db.models.User.count({}, function(err, users_count) { res.json(users_count); })
  });
  app.get('/categories/', function(req, res) { res.json(['Amsterdam', 'Washington', 'Sydney', 'Beijing', 'Cairo']); })

  app.get('/languages/', require('./views/languages/index').api);

  //front end
  app.get('/', require('./views/index').init);
  app.get('/about/', require('./views/about/index').init);
  app.get('/press/', require('./views/press/index').init);
  app.get('/troubleshoot/', require('./views/troubleshoot/index').init);
  app.get('/contact/', require('./views/contact/index').init);
  app.post('/contact/', require('./views/contact/index').sendMessage);

  //sign up
  app.get('/signup/', require('./views/signup/index').init);
  app.post('/signup/', require('./views/signup/index').signup);

  //social sign up
  app.post('/signup/social/', require('./views/signup/index').signupSocial);
  app.get('/signup/twitter/', passport.authenticate('twitter', { callbackURL: '/signup/twitter/callback/' }));
  app.get('/signup/twitter/callback/', require('./views/signup/index').signupTwitter);
  app.get('/signup/github/', passport.authenticate('github', { callbackURL: '/signup/github/callback/' }));
  app.get('/signup/github/callback/', require('./views/signup/index').signupGitHub);
  app.get('/signup/facebook/', passport.authenticate('facebook', { callbackURL: '/signup/facebook/callback/' }));
  app.get('/signup/facebook/callback/', require('./views/signup/index').signupFacebook);

  //login/out
  app.get('/login/', require('./views/login/index').init);
  app.post('/login/', require('./views/login/index').login);
  app.get('/login/forgot/', require('./views/login/forgot/index').init);
  app.post('/login/forgot/', require('./views/login/forgot/index').send);
  app.get('/login/reset/', require('./views/login/reset/index').init);
  app.get('/login/reset/:token/', require('./views/login/reset/index').init);
  app.put('/login/reset/:token/', require('./views/login/reset/index').set);
  app.get('/logout/', require('./views/logout/index').init);

  //social login
  app.get('/login/twitter/', passport.authenticate('twitter', { callbackURL: '/login/twitter/callback/' }));
  app.get('/login/twitter/callback/', require('./views/login/index').loginTwitter);
  app.get('/login/github/', passport.authenticate('github', { callbackURL: '/login/github/callback/' }));
  app.get('/login/github/callback/', require('./views/login/index').loginGitHub);
  app.get('/login/facebook/', passport.authenticate('facebook', { callbackURL: '/login/facebook/callback/' }));
  app.get('/login/facebook/callback/', require('./views/login/index').loginFacebook);

  //admin
  app.all('/admin*', ensureAuthenticated);
  app.all('/admin*', ensureAdmin);
  app.get('/admin/', require('./views/admin/index').init);

  //admin > users
  app.get('/admin/users/', require('./views/admin/users/index').find);
  app.post('/admin/users/', require('./views/admin/users/index').create);
  app.get('/admin/users/:id/', require('./views/admin/users/index').read);
  app.put('/admin/users/:id/', require('./views/admin/users/index').update);
  app.put('/admin/users/:id/password/', require('./views/admin/users/index').password);
  app.put('/admin/users/:id/role-admin/', require('./views/admin/users/index').linkAdmin);
  app.delete('/admin/users/:id/role-admin/', require('./views/admin/users/index').unlinkAdmin);
  app.put('/admin/users/:id/role-account/', require('./views/admin/users/index').linkAccount);
  app.delete('/admin/users/:id/role-account/', require('./views/admin/users/index').unlinkAccount);
  app.delete('/admin/users/:id/', require('./views/admin/users/index').delete);

  //admin > administrators
  app.get('/admin/administrators/', require('./views/admin/administrators/index').find);
  app.post('/admin/administrators/', require('./views/admin/administrators/index').create);
  app.get('/admin/administrators/:id/', require('./views/admin/administrators/index').read);
  app.put('/admin/administrators/:id/', require('./views/admin/administrators/index').update);
  app.put('/admin/administrators/:id/permissions/', require('./views/admin/administrators/index').permissions);
  app.put('/admin/administrators/:id/groups/', require('./views/admin/administrators/index').groups);
  app.put('/admin/administrators/:id/user/', require('./views/admin/administrators/index').linkUser);
  app.delete('/admin/administrators/:id/user/', require('./views/admin/administrators/index').unlinkUser);
  app.delete('/admin/administrators/:id/', require('./views/admin/administrators/index').delete);

  //admin > admin groups
  app.get('/admin/admin-groups/', require('./views/admin/admin-groups/index').find);
  app.post('/admin/admin-groups/', require('./views/admin/admin-groups/index').create);
  app.get('/admin/admin-groups/:id/', require('./views/admin/admin-groups/index').read);
  app.put('/admin/admin-groups/:id/', require('./views/admin/admin-groups/index').update);
  app.put('/admin/admin-groups/:id/permissions/', require('./views/admin/admin-groups/index').permissions);
  app.delete('/admin/admin-groups/:id/', require('./views/admin/admin-groups/index').delete);

  //admin > accounts
  app.get('/admin/accounts/', require('./views/admin/accounts/index').find);
  app.post('/admin/accounts/', require('./views/admin/accounts/index').create);
  app.get('/admin/accounts/:id/', require('./views/admin/accounts/index').read);
  app.put('/admin/accounts/:id/', require('./views/admin/accounts/index').update);
  app.put('/admin/accounts/:id/user/', require('./views/admin/accounts/index').linkUser);
  app.delete('/admin/accounts/:id/user/', require('./views/admin/accounts/index').unlinkUser);
  app.post('/admin/accounts/:id/notes/', require('./views/admin/accounts/index').newNote);
  app.post('/admin/accounts/:id/status/', require('./views/admin/accounts/index').newStatus);
  app.delete('/admin/accounts/:id/', require('./views/admin/accounts/index').delete);

  //admin > statuses
  app.get('/admin/statuses/', require('./views/admin/statuses/index').find);
  app.post('/admin/statuses/', require('./views/admin/statuses/index').create);
  app.get('/admin/statuses/:id/', require('./views/admin/statuses/index').read);
  app.put('/admin/statuses/:id/', require('./views/admin/statuses/index').update);
  app.delete('/admin/statuses/:id/', require('./views/admin/statuses/index').delete);

  //admin > categories
  app.get('/admin/categories/', require('./views/admin/categories/index').find);
  app.post('/admin/categories/', require('./views/admin/categories/index').create);
  app.get('/admin/categories/:id/', require('./views/admin/categories/index').read);
  app.put('/admin/categories/:id/', require('./views/admin/categories/index').update);
  app.delete('/admin/categories/:id/', require('./views/admin/categories/index').delete);

  //admin > search
  app.get('/admin/search/', require('./views/admin/search/index').find);

  //account
  app.all('/account*', ensureAuthenticated);
  app.all('/account*', ensureAccount);
  app.get('/account/', require('./views/account/index').init);

  // account API
  app.get('/account/my-stars/', require('./views/account/index').my_stars);
  app.get('/account/news/', require('./views/account/index').news_feed);

  //account > verification
  app.get('/account/verification/', require('./views/account/verification/index').init);
  app.post('/account/verification/', require('./views/account/verification/index').resendVerification);
  app.get('/account/verification/:token/', require('./views/account/verification/index').verify);

  //account > settings
  app.get('/account/settings/', require('./views/account/settings/index').init);
  app.put('/account/settings/', require('./views/account/settings/index').update);
  app.put('/account/settings/identity/', require('./views/account/settings/index').identity);
  app.put('/account/settings/password/', require('./views/account/settings/index').password);
  app.post('/account/settings/profile_photo/', require('./views/account/settings/index').profile_photo_upload);

  //account > settings > social
  app.get('/account/settings/twitter/', passport.authenticate('twitter', { callbackURL: '/account/settings/twitter/callback/' }));
  app.get('/account/settings/twitter/callback/', require('./views/account/settings/index').connectTwitter);
  app.get('/account/settings/twitter/disconnect/', require('./views/account/settings/index').disconnectTwitter);
  app.get('/account/settings/github/', passport.authenticate('github', { callbackURL: '/account/settings/github/callback/' }));
  app.get('/account/settings/github/callback/', require('./views/account/settings/index').connectGitHub);
  app.get('/account/settings/github/disconnect/', require('./views/account/settings/index').disconnectGitHub);
  app.get('/account/settings/facebook/', passport.authenticate('facebook', { callbackURL: '/account/settings/facebook/callback/' }));
  app.get('/account/settings/facebook/callback/', require('./views/account/settings/index').connectFacebook);
  app.get('/account/settings/facebook/disconnect/', require('./views/account/settings/index').disconnectFacebook);

  //profile
  app.get('/profile/', require('./views/profile/index').init);
  app.get('/profile/:name', require('./views/profile/index').get_profile)
  app.get('/profile/follow/:name', require('./views/profile/index').follow);
  app.get('/profile/unfollow/:name', require('./views/profile/index').unfollow);
  app.get('/profile/get-videos/:name', require('./views/profile/index').get_videos);
  app.get('/profile/get-events/:name', require('./views/profile/index').get_events);
  app.get('/profile/get-channels/:name', require('./views/profile/index').get_channels);

  //events
  app.all('/events*', ensureAuthenticated);
  app.all('/events*', ensureAccount);
  app.get('/events/', require('./views/events/index').init);
  app.get('/events/user', require('./views/events/index').my_events);
  app.post('/events/new/', require('./views/events/new_event/index').init);

  app.post('/events/new/details/', require('./views/events/new_event/index').new_event);

  //channel
  app.all('/channel*', ensureAuthenticated);
  app.all('/channel*', ensureAccount);
  app.get('/channels/', require('./views/channel/index').channels); //user channels
  app.post('/channels/', require('./views/channel/index').new_channel); // create new channel
  app.post('/channels/edit/', require('./views/channel/index').edit_channel); // edit channel
  app.post('/channels/edit/photo/', require('./views/channel/index').edit_channel_photo); // edit some channel photo
  app.get('/api/channels/', require('./views/channel/index').public_channels); // public channels


  // MCU server
  app.get('/mcu/get-room/:channelname', require('./vendor/mcu').get_room);
  app.post('/mcu/new-room/', require('./vendor/mcu').create_room);
  app.post('/mcu/new-token/', require('./vendor/mcu').create_token);
  app.get('/mcu/rooms/', require('./vendor/mcu').get_rooms);
  app.get('/mcu/users/:room', require('./vendor/mcu').get_users);


  // stream
  app.all('/stream*', ensureAuthenticated);
  app.all('/stream*', ensureAccount);
  app.get('/stream/star-this/:name', require('./views/stream/index').star_this);
  app.get('/stream/unstar-this/:name', require('./views/stream/index').unstar_this);
  app.get('/stream/deactivate/:name', require('./views/stream/index').channel_deactivate);
  app.post('/stream/update-users/', require('./views/stream/index').users_online);
  app.post('/stream/send-report/:name', require('./views/stream/index').send_report)
  app.post('/stream/stop-recording/', require('./views/stream/index').stop_recording);

  app.get('/public/:channelname', require('./views/stream/iframe/index').iframe);
  app.get('/:name', require('./views/stream/index').stream);



  //route not found
  app.all('*', require('./views/http/index').http404);
};
