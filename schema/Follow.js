'use strict';

exports = module.exports = function(app, mongoose) {

  var follow = new mongoose.Schema({
    data: { 
    	from: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
      to: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true }
    },
    followCreated: {
      id: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
      name: { type: String, default: '' },
      datetime: { type: Date, default: Date.now }
    }
  });

  app.db.model('Follow', follow);

};
