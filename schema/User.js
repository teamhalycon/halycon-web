'use strict';

exports = module.exports = function(app, mongoose) {
  var userSchema = new mongoose.Schema({
    username: { type: String, unique: true },
    password: String,
    email: { type: String, unique: true },
    photo: { type: String, default: '/media/users/profile_placeholder.gif'},
    roles: {
      admin: { type: mongoose.Schema.Types.ObjectId, ref: 'Admin' },
      account: { type: mongoose.Schema.Types.ObjectId, ref: 'Account' }
    },
    following: { type: Number, default: 0 },
    followers: { type: Number, default: 0 },
    stars: { type: Number, default: 0 },
    isActive: String,
    timeCreated: { type: Date, default: Date.now },
    resetPasswordToken: String,
    twitter: {},
    github: {},
    facebook: {},
    search: [String]
  });

  userSchema.methods.canPlayRoleOf = function(role) {
    if (role === "admin" && this.roles.admin) {
      return true;
    }

    if (role === "account" && this.roles.account) {
      return true;
    }

    return false;
  };
  userSchema.methods.defaultReturnUrl = function() {
    var returnUrl = '/';
    if (this.canPlayRoleOf('account')) {
      returnUrl = '/account/';
    }

    if (this.canPlayRoleOf('admin')) {
      returnUrl = '/admin/';
    }

    return returnUrl;
  };
  userSchema.statics.encryptPassword = function(password) {
    return require('crypto').createHmac('sha512', app.get('crypto-key')).update(password).digest('hex');
  };

  var Channel = app.db.model("Channel");
  userSchema.pre('save', function(next, done) {
    this.wasNew = this.isNew;
    next();
  });

  userSchema.post('save', function(doc) {
    if (this.wasNew) {
      var new_channel = new Channel({
        data: {
          name: doc.username
        },
        channelCreated: {
          id: doc._id,
          name: doc.username
        }
      });
      new_channel.save();
    }
  });

  userSchema.plugin(require('./plugins/pagedFind'));
  userSchema.index({ username: 1 }, { unique: true });
  userSchema.index({ email: 1 }, { unique: true });
  userSchema.index({ timeCreated: 1 });
  userSchema.index({ resetPasswordToken: 1 });
  userSchema.index({ 'twitter.id': 1 });
  userSchema.index({ 'github.id': 1 });
  userSchema.index({ 'facebook.id': 1 });
  userSchema.index({ search: 1 });
  userSchema.set('autoIndex', (app.get('env') === 'development'));
  app.db.model('User', userSchema);
};
