'use strict';

exports = module.exports = function(app, mongoose) {

  var eventSchema = new mongoose.Schema({
    data: { 
    	name: { type: String, default: '', required: true },
    	start_datetime: { type: Date, default: Date.now, required: true },
      end_datetime: { type: Date, default: Date.now, required: true },
      description: { type: String, default: '', required: true },
      type: { type: String, default: 'Public' }
    },
    channel: {
      id: { type: mongoose.Schema.Types.ObjectId, ref: 'Channel' },
      name: { type: String, default: '' }
    },
    meta: {
      folder_hash: { type: String, default: '' },
      photo_path: { type: String, default: '/media/events/events_placeholder.gif' },
      photo_thumbnail_path: { type: String, default: '/media/events/events_placeholder.gif' },
      language: { type: String, default: 'English' },
      location: { type: String, default: '' },
      status: { type: String, default: 'Upcoming' }
    },
    invoice: {
      ticket_price: { type: String, default: "Free" },
      merchant_currency: { type: String, default: 'BTC' },
      merchant: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
      buyer_currency: { type: String, default: 'BTC' },
      buyer: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
    },
    stats: {
      online_viewers: { 
        max: { type: Number, default: 0 }, // max viewers per event
        latest: { type: Date, default: Date.now }
      },
      total_views: { type: Number, default: 0 }, // total viewers across all the events which are broadcasted through this event
      stars: { type: Number, default: 0 },
      shares: { type: Number, default: 0 }
      // TODO: demographics data mining
    },
    eventCreated: {
      id: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
      name: { type: String, default: '' },
      time: { type: Date, default: Date.now }
    }
  });
  app.db.model('Event', eventSchema);

};
