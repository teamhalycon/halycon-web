'use strict';

exports = module.exports = function(app, mongoose) {

  var star = new mongoose.Schema({
    data: { 
    	channel: { type: mongoose.Schema.Types.ObjectId, ref: 'Channel', required: true },
    },
    starCreated: {
      id: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
      name: { type: String, default: '' },
      datetime: { type: Date, default: Date.now }
    }
  });

  app.db.model('Star', star);

};
