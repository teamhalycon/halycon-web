'use strict';

exports = module.exports = function(app, mongoose) {

  var channelSchema = new mongoose.Schema({
    sponsored: { type: Boolean, default: false },
    data: { 
    	name: { type: String, required: true, unique: true },
    	type: { type: String, default: 'Public' },
      photo: { type: String, default: '/media/channels/channel_placeholder.gif' }
    },
    meta: {
      default_channel: { type: Boolean, default: true },
      status: { type: String, default: 'Inactive' },
      description: { type: String, default: '' },
      language: { type: String, default: '' },
      categories: [String]
    },
    stats: {
      online_viewers: { 
        max: { type: Number, default: 0 }, // max viewers per event
        latest: { type: Date, default: Date.now }
      },
      total_views: { type: Number, default: 0 }, // total viewers across all the events which are broadcasted through this event
      stars: { type: Number, default: 0 },
      shares: { type: Number, default: 0 }
    },
    channelCreated: {
      id: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
      name: { type: String, default: '' },
      time: { type: Date, default: Date.now }
    }
  });

  channelSchema.pre('save', function(next, done) {
    //console.log("Saving channel " + this.data.name);
    next();
  });

  channelSchema.post('save', function(doc) {
    //console.log("New channel saved: " + doc);
  })

  app.db.model('Channel', channelSchema);
};
