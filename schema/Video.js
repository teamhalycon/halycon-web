'use strict';

exports = module.exports = function(app, mongoose) {

  var video = new mongoose.Schema({
    data: { 
    	channel: { type: mongoose.Schema.Types.ObjectId, ref: 'Channel', required: true },
      path: { type: String, required: true },
      type: { type: String, default: 'Public' }
    },
    videoCreated: {
      id: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
      name: { type: String, default: '' },
      datetime: { type: Date, default: Date.now }
    }
  });

  app.db.model('Video', video);

};
