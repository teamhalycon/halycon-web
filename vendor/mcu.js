"use strict";

var N = require('./nuve'),
    fs = require('fs'),
    config = require('../../licode/licode_config');


N.API.init(config.nuve.superserviceID, config.nuve.superserviceKey, 'http://localhost:3000/');

exports.get_room = function(req, res) {
    // Get room - iframe!
    var name = req.params.channelname;
    N.API.getRooms(function(roomlist) {
        "use strict";
        var rooms = JSON.parse(roomlist);
        var room_ = rooms.filter(function(obj) { return obj.name === name });
        if (room_.length > 0)
            res.json({ room: room_ });
        else
            res.json({ room: null });
    });
};

exports.create_room = function(req, res) {
    var name = req.body.name;
    N.API.getRooms(function (roomlist) {
        "use strict";
        var rooms = JSON.parse(roomlist);
        var room_ = rooms.filter(function(obj) { return obj.name === name });
        if (room_.length > 0) {
            res.json({ 'new': false, 'room': room_ });
        } else {
            N.API.createRoom(name, function(roomID) {
                console.log('Created room ', roomID);
                res.json({ 'new': true, 'room': roomID });
            });        
        }
    });    
}

exports.create_token = function(req, res) {
    "use strict";
    var room = req.body.room,
        username = req.body.username,
        role = req.body.role;

    if (req.isAuthenticated())
        username = req.user.username;

    N.API.createToken(room, username, role, function (token) {
        res.json(token);
    });
};

exports.get_rooms = function(req, res) {
    "use strict";
    console.log('Retrieving room list');
    N.API.getRooms(function (rooms) {
        res.json(JSON.parse(rooms));
    });
};
;

exports.get_users = function(req, res) {
    "use strict";
    var room = req.params.room;
    N.API.getUsers(room, function (users) {
        res.json(JSON.parse(users));
    });
};