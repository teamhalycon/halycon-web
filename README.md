halycon.tv
=============

Realtime VDR platform with digital currency transaction support. [See a bird's eye view.](http://en.wikipedia.org/wiki/Virtual_data_room)



Dependecies
------------


| On The Server | On The Client  | Development |
| ------------- | -------------- | ----------- |
| Express       | Bootstrap      | Grunt       |
| Jade          | Backbone.js    |             |
| Mongoose      | jQuery         |             |
| Passport      | Underscore.js  |             |
| Async         | Font-Awesome   |             |
| EmailJS       | Moment.js      |             |



Requirements
------------

You need [Node.js](http://nodejs.org/download/) and [MongoDB](http://www.mongodb.org/downloads) installed and running.

We use [Grunt](http://gruntjs.com/) as our task runner. Get the CLI (command line interface).

```bash
$ npm install -g grunt-cli
```

WARNING!!!!! Also ALWAYS INSTALL GRAPHICSMAGICS ON MACHINE.


Installation
------------

```bash
$ git clone https://jsam@bitbucket.org/teamhalycon/halycon-web.git && cd halycon-web
$ npm install
```

Setup - Initialization for devel enviroment
-------------------------------------------

You need a few records in the database to start using the user system.

Run these commands on mongo. __Obviously you should use your email address.__

```js
use drywall;
db.admingroups.insert({ _id: 'root', name: 'Root' });
db.admins.insert({ name: {first: 'Root', last: 'Admin', full: 'Root Admin'}, groups: ['root'] });
var rootAdmin = db.admins.findOne();
db.users.save({ username: 'root', isActive: 'yes', email: 'your@email.addy', roles: {admin: rootAdmin._id} });
var rootUser = db.users.findOne();
rootAdmin.user = { id: rootUser._id, name: rootUser.username };
db.admins.save(rootAdmin);
```

Now just use the reset password feature to set a password.

 - `http://localhost:3000/login/forgot/`
 - Submit your email address and wait a second.
 - Go check your email and get the reset link.
 - `http://localhost:3000/login/reset/:token/`
 - Set a new password.

Login && Enjoy.

 
Features
------------
 - virtual data rooms with channel support
 - schedule event
 - subscribe to channel's
 - buy and sell ticket's (digital currency support)


License
---------

	halycon.tv, Inc Licenced