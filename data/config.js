'use strict';

exports.domain_name = "https://halycon.tv";
exports.port = process.env.PORT || 443;
exports.mongodb = {
  uri: process.env.MONGOLAB_URI || process.env.MONGOHQ_URL || 'localhost/halycon'
};
exports.companyName = 'Halycon.TV, Inc.';
exports.projectName = 'Halycon.TV';
exports.systemEmail = 'admin@halycon.tv';
exports.cryptoKey = 'k3yb0ardc4t';
exports.requireAccountVerification = false;
exports.smtp = {
  from: {
    name: process.env.SMTP_FROM_NAME || exports.projectName,
    address: process.env.SMTP_FROM_ADDRESS || 'support@halycon.tv'
  },
  credentials: {
    user: process.env.SMTP_USERNAME || 'suport.halycon.team',
    password: process.env.SMTP_PASSWORD || 'Support@Halycon#1337',
    host: process.env.SMTP_HOST || 'smtp.gmail.com',
    ssl: true
  }
};

exports.oauth = {
  twitter: {
    key: process.env.TWITTER_OAUTH_KEY || 'BcQDDe3rS2HmWzvPdSQBxg',
    secret: process.env.TWITTER_OAUTH_SECRET || 'rQjUp02Xv568mYowgATdNyCWAz7067ocJ71lKFHY4'
  },
  facebook: {
    key: process.env.FACEBOOK_OAUTH_KEY || '211979585660463',
    secret: process.env.FACEBOOK_OAUTH_SECRET || 'ecdae4d6ef3efc392855104d3ec3154a'
  },
  github: {
    key: process.env.GITHUB_OAUTH_KEY || '',
    secret: process.env.GITHUB_OAUTH_SECRET || ''
  }
};
