var share = (function(x, $) {
	var endpoint = {
		email: {
			url: 'mailto:?subject={HEAD}&body={SUMM}%20{URL}'
		},
		facebook: {
			url: 'https://facebook.com/sharer.php?s=100&p[url]={URL}&p[images][0]={IMAGE}&p[title]={HEAD}&p[summary]={SUMM}'
		},
		twitter: {
			url: 'https://twitter.com/intent/tweet?url={URL}&text={SUMM}'
		},
		linkedin: {
			url: 'https://linkedin.com/shareArticle?mini=true&url={URL}&title={HEAD}&summary={SUMM}&source=Halycon.TV'
		},
		googleplus: {
			url: 'https://plus.google.com/share?url={URL}'
		}
	};

	var events = {
		onClickShareToggle: function(e) {
			e.preventDefault();
			var anchor = $(this);
			var options = {
				network: anchor.data('network'),
				url: anchor.attr('href'),
				head: anchor.data('head'),
				summ: anchor.data('summ'),
				image: anchor.data('image')
			};
			var url = getShareUrl(options);
			popup(url);
		}
	};

	var getShareUrl = function(options) {
		var url = false;
		$.each(endpoint, function(network, element) {
			if(network === options.network) {
				url = element.url;
				if(typeof options.url !== 'undefined') {
					url = url.replace('{URL}', encodeURIComponent(options.url));
				}
				else {
					url = url.replace('{URL}', '');
				}
				if(typeof options.head !== 'undefined') {
					url = url.replace('{HEAD}', encodeURIComponent(options.head));
				}
				else {
					url = url.replace('{HEAD}', '');
				}
				if(typeof options.summ !== 'undefined') {
					url = url.replace('{SUMM}', encodeURIComponent(options.summ));
				}
				else {
					url = url.replace('{SUMM}', '');
				}
				if(typeof options.image !== 'undefined') {
					url = url.replace('{IMAGE}', encodeURIComponent(options.image));
				}
				else {
					url = url.replace('{IMAGE}', '');
				}
			}
		});
		return url;
	};

	var onDocumentReady = function() {
		$(document).on('click', '.share-toggle', events.onClickShareToggle);
	};

	var popup = function(url) {
		var width = 600;
		var height = 500;
		var left = (screen.width) ? (screen.width - width) / 2 : 0;
		var top = (screen.height) ? (screen.height - height) / 2 : 0;
		var options = ',location=0,menubar=0,resizable=0,status=0,titlebar=0,toolbar=0';
		window.open(url, '_blank', 'height=' + height + ',width=' + width + ',left=' + left + ',top=' + top + options);
	};

	$(onDocumentReady);
	return x;
}(share || {}, jQuery));
