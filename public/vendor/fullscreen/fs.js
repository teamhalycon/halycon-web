var videoElement = document.getElementById("main_display");

function toggleFullScreen() {
	if(!document.fullscreenElement && !document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement ) {
		if(videoElement.requestFullscreen) {
			videoElement.requestFullscreen();
		} else if(videoElement.msRequestFullscreen) {
			videoElement.msRequestFullscreen();
		} else if(videoElement.mozRequestFullScreen) {
			videoElement.mozRequestFullScreen();
		} else if(videoElement.webkitRequestFullscreen) {
			videoElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
		}
	} else {
		if(document.exitFullscreen) {
			document.exitFullscreen();
		} else if(document.msExitFullscreen) {
			document.msExitFullscreen();
		} else if(document.mozCancelFullScreen) {
			document.mozCancelFullScreen();
		} else if(document.webkitExitFullscreen) {
			document.webkitExitFullscreen();
		}
	}
}

videoElement.addEventListener("dblclick", function(e) {
	toggleFullScreen();
}, false);
