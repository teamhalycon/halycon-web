var PhotoMe = (function() {

    var streaming    = false,
        media_stream       = null,
        video        = document.querySelector('#video'),
        video_block  = document.querySelector('#video-block'),
        canvas       = document.querySelector('#canvas'),
        canvas_block = document.querySelector('#canvas-block'),
        photo        = document.querySelector('#profile_img'),
        startbutton  = document.querySelector('#startbutton'),
        width        = 320,
        height       = 0;

    navigator.getMedia = ( navigator.getUserMedia ||
                           navigator.webkitGetUserMedia ||
                           navigator.mozGetUserMedia ||
                           navigator.msGetUserMedia);

    var stop = function() {
      //media_stream.stop();
      streaming = false;
    }

    var start = function() {
      hideshow(canvas_block, video_block);
      navigator.getMedia(
        {
          video: true,
          audio: false
        },
        function(stream) {
          media_stream = stream;
          if (navigator.mozGetUserMedia) {
            video.mozSrcObject = stream;
          } else {
            var vendorURL = window.URL || window.webkitURL;
            video.src = vendorURL.createObjectURL(stream);
          }
          video.play();
        },
        function(err) {
          console.log("An error occured! " + err);
        }
      );
    }
    
    video.addEventListener('canplay', function(ev){
      if (!streaming) {
        height = video.videoHeight / (video.videoWidth/width);
        video.setAttribute('width', width);
        video.setAttribute('height', height);
        canvas.setAttribute('width', width);
        canvas.setAttribute('height', height);
        streaming = true;
      }
    }, false);

    var takepicture = function() {
      canvas.width = width;
      canvas.height = height;
      canvas.getContext('2d').drawImage(video, 0, 0, width, height);
      var data = canvas.toDataURL('image/png');
      photo.setAttribute('src', data);

      hideshow(video_block, canvas_block);
      
      stop();
      startbutton.innerHtml = 'Take another photo';
    }

    var hide = function(e) {
      e.style.display = "none";
    }

    var show = function(e) {
      e.style.display = "block";
    }

    var hideshow = function(e1, e2) {
      hide(e1); 
      show(e2);
    }

    hideshow(canvas_block, video_block);

    startbutton.addEventListener('click', function(ev) {
        if (streaming) {
          takepicture();  
        } else {
          start();
        }
      ev.preventDefault();
    }, false);



}());