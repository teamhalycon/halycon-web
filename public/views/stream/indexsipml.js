(function(){
    'use strict';
    
    var init_scroller = function() {
        $('.nano').nanoScroller({
            preventPageScrolling: true,
            sliderMaxHeight: 15,
            scroll: 'top'
        });         
    };

    var loading = function(action) {
        var opts = {
          lines: 13, // The number of lines to draw
          length: 20, // The length of each line
          width: 10, // The line thickness
          radius: 30, // The radius of the inner circle
          corners: 1, // Corner roundness (0..1)
          rotate: 0, // The rotation offset
          direction: 1, // 1: clockwise, -1: counterclockwise
          color: '#000', // #rgb or #rrggbb or array of colors
          speed: 1, // Rounds per second
          trail: 60, // Afterglow percentage
          shadow: false, // Whether to render a shadow
          hwaccel: false, // Whether to use hardware acceleration
          className: 'spinner', // The CSS class to assign to the spinner
          zIndex: 2e9, // The z-index (defaults to 2000000000)
          top: 'auto', // Top position relative to parent in px
          left:'auto' // Left position relative to parent in px
        };

        var target = document.getElementById('loading_spinner');
        var spinner = new Spinner(opts).spin(target);
        $("#loading").modal("toggle");
        console.log("#loading toggled");
        /*
        if ($('modal-backdrop')) {
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();    
        }*/
    };
    

    //////////////////////
    //  Telepresence
    //////////////////////
    var tp = {};
    tp.started = false;
    var readyCallback = function(e) {
        createSipStack();
    };

    var errorCallback = function(e) {
        console.error('Failed to initialize error stack' + e.message);
        window.location = "/";
    };

    var eventsListener = function(e) {
        console.log('eventsListener', e);
        if (e.type == 'started') {
            //login();
            loading();
            tp.started = true;
        } else if (e.type == 'i_new_message') {
            // incoming new SIP message (sms-like)
            acceptMessage(e);
        } else if (e.type == 'i_new_call') {
            // incoming audio/video call
            acceptCall(e);
        } else if (e.type == 'connected') {
            // in call 
            //makeCall();
        }
    };

    var sipStack, sipConfig = {
        realm: 'halycon0.co.vu',
        impi: 'anonymous',
        impu: 'sip:anonymous@halycon0.co.vu',
        password: 'mysecret',
        mute: false,
        display_name: 'anonymous',
        websocket_proxy_url: 'ws://95.85.6.252:20060',
        outbound_proxy_url: 'udp://95.85.6.252:20060',
        //ice_servers: [{ url: 'stun:stun.l.google.com:19302'}, { url:'turn:user@numb.viagenie.ca', credential:'myPassword'}], // optional
        bandwidth: { audio: 64, video: 144 }, // optional
        video_size: { minWidth: 865, minHeight: 536, maxWidth: 1920, maxHeight: 1080 },
        enable_rtcweb_breaker: false, // optional
        enable_click2call: false, // optional
        events_listener: { events: '*', listener: eventsListener },
        sip_headers: [
            { name: 'User-Agent', value: 'IM-client/OMA1.0 sipML5-v1.0.89.0' },
            { name: 'Organization', value: "halycon.tv" }
        ]
    };


    var createSipStack = function() {
        sipStack = new SIPml.Stack(sipConfig);
        sipStack.start();
        tp.started = true;
    }


    var callSession;
    var makeCall = function(type){
        callSession = sipStack.newSession(type, {
            video_local: null,//document.getElementById('my-video-remote'), // 
            video_remote: document.getElementById('main-display-video'), // TODO:
            audio_remote: document.getElementById('main-audio-remote'), // TOOD:
            events_listener: { events: '*', listener: eventsListener } // optional: '*' means all events
        });
        callSession.call($('#channel_name').html());
    }    


     var acceptCall = function(e) {
        e.newSession.accept(); // e.newSession.reject() to reject the call
    }



    var app = angular.module('app', []);

    app.controller('qs_cntrl', function($scope, $http) {
        // TODO: disconect from sip stack
        $scope.quickstream = function() {
            window.location = '/' + $('#qs_input').val().replace(/\s+/g, "-").toLowerCase();
        }
    });
    app.controller('info', function($scope, $http) {});
    app.controller('share', function($scope, $http) {});
    app.controller('report', function($scope, $http) {});
    app.controller('users', function($scope, $http) {
        $scope.remove_element = function(stream, index) {
            
        };
        $scope.screen_swap = function(stream, index) {
            
        };
    });
    app.controller('chat', function($scope, $http) {
        
    });
    app.controller('settings', function($scope, $http) {
        $scope.share_camera = function() {
            init_camera($scope);
        };
        $scope.share_desktop = function() {
            init_desktop($scope); 
        };
    });
    app.controller('main', function($scope, $http) {
        $scope.username = 
            sipConfig.impi =
            sipConfig.display_name = $("#username").html().replace(/ /g,'');
        
        sipConfig.impu = "sip:" + $scope.username + "@halycon0.co.vu";

        $scope.channel = { data: { name: $('#channel_name').html() } };
        $scope.main_display = null;

        $scope.user_streams = [];
        $scope.chat_streams = [];
        $scope.messages = [];
        $scope.new_message = "";
    });

    var init_room = function($scope, loading) {

    }

    var init_desktop = function($scope) {
        
    };

    var init_camera = function($scope) {
        if (tp.started)
            makeCall('call-audiovideo');
    };
    
    var init_chat = function($scope) {
        if (tp.started)
            makeCall('call-screenshare');
    };

    

    

    $(document).ready(function() {
        init_scroller();
        $('#users_btn').click();
        $('#share_btn').click();
        loading();
        SIPml.init(readyCallback, errorCallback);
    });
}());