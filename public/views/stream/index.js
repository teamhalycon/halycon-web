(function(){
    'use strict';

    var app = angular.module('app', []);

    app.controller('qs_cntrl', function($rootScope, $http) {
        $rootScope.quickstream = function() {
            window.location = '/' + $('#qs_input').val().replace(/\s+/g, "-").toLowerCase();
        }
    });

    app.controller('info', function($rootScope, $http) {});
    app.controller('share', function($rootScope, $http) {});
    app.controller('report', function($rootScope, $http) {});

    app.controller('users', function($rootScope, $http) {
        $rootScope.$watch('main_display', function(new_stream, old_stream) {
            // $watch which will put main_display content to userland
            if (old_stream) {
                $rootScope.user_streams.push(old_stream);
                setTimeout(function() {
                    old_stream.show(old_stream.timestamp);
                });
            }
        });

        $rootScope.remove_element = function(stream, index) {
            stream.hide();
            $rootScope.user_streams.splice(index, 1);
            var local = false;
            // TODO: Test this part
            if ($rootScope.local_stream)
                if (stream.getID() == $rootScope.local_stream.getID()) {
                    console.log("Localstream unpublished");
                    $rootScope.room.unpublish($rootScope.local_stream);
                    $rootScope.local_stream_published = false;
                    $rootScope.local_stream.close();
                    local = true;
                }
            if ($rootScope.desktop_stream)
                if (stream.getID() == $rootScope.desktop_stream.getID()) {
                    console.log("Desktopstream unpublished");
                    $rootScope.room.unpublish($rootScope.desktop_stream);
                    $rootScope.desktop_stream_published = false;
                    $rootScope.desktop_stream.close();
                    local = true;
                }
            if (!local) {
                console.log("Stream unsubscribed");
                $rootScope.room.unsubscribe(stream);
            }
        };
        $rootScope.screen_swap = function(stream, index) {
            console.log("Screen swap: ", $rootScope.user_streams);
            //$rootScope.remove_element(stream, index);
            stream.hide();
            $rootScope.user_streams.splice(index, 1);
            if ($rootScope.main_display != null) {
                console.log("main_display object is not null");
                $rootScope.main_display.main_display = false;
                stream.main_display = true;
                $rootScope.main_display.hide();
            }

            $rootScope.main_display = stream;
            $('#main_display').html("");
            $rootScope.main_display.show('main_display');


        };

    });
    app.controller('chat', function($rootScope, $http) {
        $rootScope.send_message = function() {
            console.log('Sending message', $rootScope.nmsg.new_message);
            if ($rootScope.nmsg.new_message !== "") {
                $rootScope.chat.sendData({ msg: $rootScope.nmsg.new_message, timestamp: new Date() });
                $rootScope.nmsg.new_message = "";
            }
        }
    });

    app.controller('settings', function($rootScope, $http) {
        $rootScope.share_camera = function(video) {
            init_camera($rootScope, video);
        };
        $rootScope.share_desktop = function() {
            init_desktop($rootScope);
        };
    });

    app.controller('main', function($rootScope, $http, $sce) {

        $rootScope.local_stream_published = false;
        $rootScope.desktop_stream_published = false;

        $rootScope.camera_recording = false;
        $rootScope.desktop_recording = false;

        $rootScope.username = $("#username").html().replace(/ /g,'');
        $rootScope.channel = {
          data: { name: $('#channel_name').html() },
          stats: { online_viewers: { max: 0 } }
        };

        $rootScope.owner = $rootScope.username === $rootScope.channel.data.name ? true: false;

        $rootScope.url_share = "https://halycon.tv/profile/" + $rootScope.username;
        $rootScope.iframe = {
            src: "https://halycon.tv/public/" + $rootScope.channel.data.name,
            name: $rootScope.channel.data.name,
            width: 830,
            height: 515
        };
        $rootScope.iframe.string = "<iframe src='" + $rootScope.iframe.src
                                    + "' height='" + $rootScope.iframe.height
                                    + "' width='" + $rootScope.iframe.width
                                    + "' name='" + $rootScope.iframe.name + "'"
                                    + " scrolling='no' align='center' frameborder='0' marginheight='0' marginwidth='0' allowFullScreen></iframe>"

        $rootScope.main_display = null;

        $rootScope.user_streams = [];
        $rootScope.chat_streams = [];
        $rootScope.messages = [];
        $rootScope.nmsg = {
            new_message: ""
        }

        $rootScope.local_stream = null;
        $rootScope.desktop_stream = null;

        $rootScope.starred = $("#starred").val();
        console.log($rootScope.starred);

        $rootScope.report = { title: "", description: "" };

        $http.post('/mcu/new-room/', { name: $rootScope.channel.data.name  })
            .success(function(data, status, headers, config) {
                $rootScope._room = data;
                console.log($rootScope._room);
                $rootScope.role = 'presenter'; // 'subscriber'
                $http.post('/mcu/new-token/', { room: $rootScope._room.room[0]._id, role: $rootScope.role })
                    .success(function(data, status, headers, config) {
                        $rootScope.token = data;
                        init_room($rootScope, loading);
                        //init_camera($rootScope);
                    });
            });



        $rootScope.published_toggle_camera = function() {
            $rootScope.$apply(function() {
                $rootScope.local_stream_published = !$rootScope.local_stream_published;
            });
        }

        $rootScope.published_toggle_desktop = function() {
            $rootScope.$apply(function() {
                $rootScope.desktop_stream_published = !$rootScope.desktop_stream_published;
            });
        };

        $rootScope.stop_rec = function(url) {
            $http.post('/stream/stop-recording/', { video_path: url, channel: $rootScope.channel.data.name })
                .success(function(resp, status, headers, config) {
                    console.log(resp);
                });
        }

        $rootScope.record_camera = function() {
            console.log('recording camera');
            $rootScope.ladda_camera = Ladda.create($("#rec_camera")[0]);
            $rootScope.ladda_camera.start();
            $rootScope.camera_recording = true;

            $rootScope.room.startRecording($rootScope.local_stream);
        };

        $rootScope.stop_recording_camera = function() {
            $rootScope.camera_recording = false;
            $rootScope.ladda_camera.stop();
            $rootScope.room.stopRecording($rootScope.local_stream,
                function(object) {
                    console.log(object);
                    $rootScope.stop_rec(object.url);
                });
        };

        $rootScope.record_desktop = function() {
            console.log('recording desktop');
            $rootScope.ladda_desktop = Ladda.create($("#rec_desktop")[0]);
            $rootScope.ladda_desktop.start();
            $rootScope.desktop_recording = true;
            $rootScope.room.startRecording($rootScope.desktop_stream);
        };

        $rootScope.stop_recording_desktop = function() {
            $rootScope.desktop_recording = false;
            $rootScope.ladda_desktop.stop();
            $rootScope.room.stopRecording($rootScope.desktop_stream,
                function(object) {
                    console.log(object);
                    $rootScope.stop_rec(object.url);
                });
        };

        $rootScope.render = function(html) {
            return $sce.trustAsHtml(html);
        };

        $rootScope.star_this = function() {
          var url = '/stream/star-this/' + $rootScope.channel.data.name;
          console.log(url);
          $http.get(url)
            .success(function(data, status, headers, config) {
              if (data.message = "ok") {
                $('#channel_stars').html(data.stars);
                $rootScope.starred = true;
              }
            });
        };

        $rootScope.unstar_this = function() {
          console.log('unstar_this');
          var url = '/stream/unstar-this/' + $rootScope.channel.data.name;
          $http.get(url)
            .success(function(data, status, headers, config) {
              console.log(data);
              if (data.message == "ok") {
                $('#channel_stars').html(data.stars);
                $rootScope.starred = false;
              }
            })
        };

        $rootScope.get_users_online = function() {
          var url = '/mcu/users/' + $rootScope._room.room[0]._id;
          $http.get(url)
            .success(function(data, status, headers, config) {

              console.log(data);
              $rootScope.channel.stats.online_viewers.max = data.length;
              console.log($rootScope.channel.stats.online_viewers.max);

              var url_save = '/stream/update-users/';
              $http.post(url_save,
                { packet: { channel: $rootScope.channel.data.name, users_online: data.length }})
                .success(function(data, status, headers, config) {
                  console.log("Max viewers updated: ", data);
                });
            });
        };

        $rootScope.send_report = function() {
          var url = '/stream/send-report/' + $rootScope.channel.data.name;
          $http.post(url, { report: $rootScope.report })
            .success(function(data, status, headers, config) {
              console.log(data);
              $rootScope.report.title = "";
              $rootScope.report.description = "";
            });
        };

        $rootScope.deactivate = function() {
          var url = '/stream/deactivate/' + $rootScope.channel.data.name;
          $http.get(url)
            .success(function(data, status, headers, config) {
              console.log(data);
              if (data.message == "ok") {
                window.location = "/";
              }
            });
        };
    });

    var init_room = function($rootScope, loading) {

        $rootScope.room = Erizo.Room({ token: $rootScope.token });

            var subscribe_toStreams = function(streams) {
                var flag;
                for (var index in streams) {
                    flag = true;
                    var stream = streams[index];

                    if ($rootScope.local_stream)
                        if ($rootScope.local_stream.getID() === stream.getID())
                            flag = false;

                    if ($rootScope.desktop_stream)
                        if ($rootScope.desktop_stream.getID() === stream.getID())
                            flag = false;

                    if (flag)
                        $rootScope.room.subscribe(stream);
                }
            };

            var room_connected = function(room_event) {

                subscribe_toStreams(room_event.streams);
                //loading('patch-ui');
                $('#processing-modal').modal('hide');
                $rootScope.get_users_online();
                init_chat($rootScope);
            };

            var stream_subscribed = function(stream_event) {

                var stream = stream_event.stream;
                stream.attr = stream.getAttributes();

                if (stream.attr.type !== "Chat") {
                    $rootScope.$apply(function() {
                        stream.timestamp = Date.now();
                        stream.main_display = false;
                        $rootScope.user_streams.push(stream);
                    });
                    stream.show(stream.timestamp);
                } else {
                    stream.addEventListener('stream-data', function(evt) {
                        $rootScope.$apply(function() {
                            var parsed_msg = replaceURLWithHTMLLinks(evt.msg.msg);
                            $rootScope.messages.push({
                                msg: parsed_msg,
                                from: evt.stream.getAttributes().name,
                                timestamp: {
                                    datetime: evt.msg.timestamp,
                                    format: moment().calendar()
                                }
                            });
                            $("#chat_nano").nanoScroller({ scroll: 'bottom' });
                        });

                        console.log($rootScope.messages);
                    });
                }
            };

            var stream_added = function(stream_event) {
                var streams = [];
                streams.push(stream_event.stream);
                console.log(stream_event.stream);
                console.log(stream_event.stream.hasScreen())
                subscribe_toStreams(streams);
                $rootScope.get_users_online();
            };

            var stream_removed = function(stream_event) {
                var stream = stream_event.stream;
                if (stream.elementID !== undefined) {
                    for (var i = 0; i < $rootScope.user_streams.length; ++i)
                        if ($rootScope.user_streams[i].elementID == stream.elementID)
                            break;
                    $rootScope.$apply(function() {
                        $rootScope.user_streams.splice(i, 1);
                        stream.hide();
                    });
                    $rootScope.get_users_online();
                }
            };

            $rootScope.room.addEventListener('room-connected', room_connected);
            $rootScope.room.addEventListener('stream-subscribed', stream_subscribed);
            $rootScope.room.addEventListener('stream-added', stream_added);
            $rootScope.room.addEventListener('stream-removed', stream_removed);

            $rootScope.room.connect();
    }


    var init_desktop = function($rootScope) {
        $rootScope.desktop_stream = Erizo.Stream({ screen: true, data: true, videoSize: [640, 480, 640, 480],
            attributes: { name: $rootScope.username, type: "Desktop" }});
        $rootScope.desktop_stream.attr = { type: 'Desktop', name: $rootScope.username };

        $rootScope.desktop_stream.init();
        $rootScope.desktop_stream.addEventListener('access-accepted', function() {
            $rootScope.room.publish($rootScope.desktop_stream, { maxVideoBW: 300 });
            $rootScope.published_toggle_desktop();

            // Update UI
            $rootScope.$apply(function() {
                $rootScope.desktop_stream.timestamp = Date.now();
                $rootScope.desktop_stream.main_display = false;
                $rootScope.user_streams.push($rootScope.desktop_stream);
            });
            $rootScope.desktop_stream.show($rootScope.desktop_stream.timestamp);
        });
        $rootScope.desktop_stream.addEventListener('access-denied', function() {});
    };

    var init_camera = function($rootScope, _video) {
        var _attributes = { name: $rootScope.username, type: "Camera" };
        if (!_video) 
            _attributes.type = "Microphone";

        $rootScope.local_stream = Erizo.Stream({ audio: true, video: _video,
            attributes: _attributes });
        $rootScope.local_stream.attr = _attributes;

        $rootScope.local_stream.init();
        $rootScope.local_stream.addEventListener('access-accepted', function() {
            $rootScope.room.publish($rootScope.local_stream);
            if (_video)
                $rootScope.published_toggle_camera();

            // Update UI
            $rootScope.$apply(function() {
                $rootScope.local_stream.timestamp = Date.now();
                $rootScope.local_stream.main_display = false;
                $rootScope.user_streams.push($rootScope.local_stream);
            });
            $rootScope.local_stream.show($rootScope.local_stream.timestamp);
            if (!_video) {
                $('#' + $rootScope.local_stream.timestamp).css("background-image", "url(/assets/microphone.svg)");
                $('#' + $rootScope.local_stream.timestamp).css("background-size", "100% 100%");
            }
        });
        $rootScope.local_stream.addEventListener('access-denied', function() {});
    };

    var init_chat = function($rootScope) {
        $rootScope.chat = Erizo.Stream({ data: true, attributes:
            { name: $rootScope.username, type: "Chat" }});
        $rootScope.chat.init();
        $rootScope.room.publish($rootScope.chat);
        console.log("Chat published");
    };


    var init_scroller = function() {
        $('.nano').nanoScroller({
            preventPageScrolling: true,
            sliderMaxHeight: 15,
            scroll: 'top'
        });
    };

    var loading = function(action) {
        var opts = {
          lines: 13, // The number of lines to draw
          length: 20, // The length of each line
          width: 10, // The line thickness
          radius: 30, // The radius of the inner circle
          corners: 1, // Corner roundness (0..1)
          rotate: 0, // The rotation offset
          direction: 1, // 1: clockwise, -1: counterclockwise
          color: '#000', // #rgb or #rrggbb or array of colors
          speed: 1, // Rounds per second
          trail: 60, // Afterglow percentage
          shadow: false, // Whether to render a shadow
          hwaccel: false, // Whether to use hardware acceleration
          className: 'spinner', // The CSS class to assign to the spinner
          zIndex: 2e9, // The z-index (defaults to 2000000000)
          top: 'auto', // Top position relative to parent in px
          left:'auto' // Left position relative to parent in px
        };



        var target = document.getElementById('loading_spinner');
        var spinner = new Spinner(opts).spin(target);
        $("#loading").modal("toggle");

        if (action == 'patch-ui') {
            $('body').removeClass('modal-open')
            $('.modal-backdrop').remove();
        }
    };

    function replaceURLWithHTMLLinks(text) {
        var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
        return text.replace(exp,"<a href='$1' target='_blank'>$1</a>");
    }


    $(document).ready(function() {
        $('#processing-modal').modal('show');
        init_scroller();
        $('#users_btn').click();
        $('#share_btn').click();
        //loading();
    });
}());
