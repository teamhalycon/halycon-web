(function() {
  'use strict';
  
  var app = angular.module('app', []);

  app.controller('main', function($rootScope, $http) {
    
    $rootScope.channel_name = $('#channel_name').val();
    $rootScope.channel_owner = $('#channel_owner').val();

    $rootScope.connect = function() {
      $http.get('/mcu/get-room/' + $rootScope.channel_name)
        .success(function(data, status, headers, config) {
          console.log(data);
          if (data.room != null) {
            $rootScope._room = data.room[0];
            $rootScope.role = 'viewer';
            $http.post('/mcu/new-token/', { room: $rootScope._room._id, role: $rootScope.role })
              .success(function(data, status, headers, config) {
                $rootScope.token = data;
                init_room($rootScope);
            });
          }
      });
    };

  });
  
  
  var init_room = function($rootScope) {
    $rootScope.room = Erizo.Room({ token: $rootScope.token });

    var subscribe_toStreams = function(streams) {
      for (var index in streams) {
        var stream = streams[index];
        var attr = stream.getAttributes();
        console.log(attr);
        if ($rootScope.channel_owner == attr.name && attr.type != "Chat") {
          $rootScope.room.subscribe(stream);
        }
      }
    };

    var room_connected = function(room_event) {
      subscribe_toStreams(room_event.streams);
      console.log(room_event.streams);
    };

    var stream_subscribed = function(stream_event) {
      var stream = stream_event.stream;
      $('#channel_photo').hide();
      stream.show('main_display');

      /*
                var stream = stream_event.stream;
                stream.attr = stream.getAttributes();

                if (stream.attr.type !== "Chat") {
                    $rootScope.$apply(function() {
                        stream.timestamp = Date.now();
                        stream.main_display = false;
                        $rootScope.user_streams.push(stream);
                    });
                    stream.show(stream.timestamp);
                } else {
                    stream.addEventListener('stream-data', function(evt) {
                        $rootScope.$apply(function() {
                            var parsed_msg = replaceURLWithHTMLLinks(evt.msg.msg);
                            $rootScope.messages.push({ 
                                msg: parsed_msg, 
                                from: evt.stream.getAttributes().name, 
                                timestamp: { 
                                    datetime: evt.msg.timestamp, 
                                    format: moment().calendar() 
                                }
                            });
                            $("#chat_nano").nanoScroller({ scroll: 'bottom' });
                        });
                        
                        console.log($rootScope.messages);
                    });
                }
        */
    };

    var stream_added = function(stream_event) {
      var streams = [];
      streams.push(stream_event.stream);
      subscribe_toStreams(streams);
    };

    var stream_removed = function(stream_event) {
      $('#channel_photo').show();
    };

    $rootScope.room.addEventListener('room-connected', room_connected);
    $rootScope.room.addEventListener('stream-subscribed', stream_subscribed);
    $rootScope.room.addEventListener('stream-added', stream_added);
    $rootScope.room.addEventListener('stream-removed', stream_removed);

    $rootScope.room.connect();
  }


  $(document).ready(function() {
    var el = $('#channel_status');
    if (el.val() == "Active" && $('#channel_type').val() == "Public") {
      $('#channel_photo').click();
    }
  });



}());