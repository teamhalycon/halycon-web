$(document).ready(function() {
	$('#multipoint').click(function(e) {
		e.preventDefault();
		$('#multipoint').ekkoLightbox();
	});

	$('#accept_camera').click(function(e) {
		e.preventDefault();
		$('#accept_camera').ekkoLightbox();
	});

	$('#share_desktop').click(function(e) {
		e.preventDefault();
		$('#share_desktop').ekkoLightbox();
	});
});
