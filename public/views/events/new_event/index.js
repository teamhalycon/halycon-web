(function() {
  'use strict';


  var app = angular.module('app', []);
  app.controller('qs_cntrl', function($scope, $http) {
    
  });
  app.controller('new_event', function($scope, $http) {
    $scope.channels = [];
    $scope.event_schedule = "";

    $scope.name = $('#event_name').html();
    $scope.description = '';
    $scope.channel_selected = {};

    $scope.selected = function(item) { $scope.channel_selected = item; };
    $scope.get_channels = function(done) {
            $http.get('/channels/').success(function(data, status, headers, config) { done(data); });
    };
    $scope.get_channels(function(data) { $scope.channels = data; $scope.channel_selected = data[0]; });

    $scope.save_event = function(done) {
      console.log('saving event');
      var formData = new FormData($("#create_event_form")[0]);
      formData.append('name', $scope.name);
      var cropper = { 
        cropX: sessionStorage.getItem('cropX'),
        cropY: sessionStorage.getItem('cropY'),
        cropW: sessionStorage.getItem('cropW'),
        cropH: sessionStorage.getItem('cropH')
      }

      // check if promo photo was selected if it is append cropper to formData
      if (cropper.cropX != null &&
          cropper.cropY != null &&
          cropper.cropW != null &&
          cropper.cropH != null) {
        formData.append('cropper', JSON.stringify(cropper));
        formData.append('promo_photo', JSON.stringify(true));
      } else {
        formData.append('promo_photo', JSON.stringify(false));
      }
      formData.append('channel', JSON.stringify($scope.channel_selected));
      
      
      var dt = $('#event_schedule').val().split('-');
      formData.append('start_datetime', JSON.stringify(new Date(dt[0])));
      formData.append('end_datetime', JSON.stringify(new Date(dt[1])));
      console.log(dt)
      console.log(Date(dt[0])); 
      console.log(Date(dt[1]));
      
      // TODO: validation
      if ($scope.description && dt) {

        var l = Ladda.create($("#save_event_button")[0]); 
        l.start();
        
        var form = new FormUpload({
          url: '/events/new/details/',
          data: formData,
          beforeSendHandler: function() {},
          completeHandler: function(data) {
            l.stop();
            window.location = '/account/';
          },
          errorHandler: function() {},
          progressHandler: function(e) {
            if(e.lengthComputable)
            {
              var percentComplete = e.loaded / e.total;
              l.setProgress(percentComplete);
            }
          },
        });
        form.upload();
      }


      
    }
    $scope.save_event_callback = function(data) {
      console.log(data);
    }

  });

  var start_date = new Date();
  start_date.setDate(start_date.getDate());
  console.log(start_date);
  $('#event_schedule').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' });

  var init_cropper = function() {
    $('.crop_me').jWindowCrop({
      targetWidth: 830,
      targetHeight: 515,
      loadingText: 'Promo photo',
      onChange: function(result) {
        sessionStorage.setItem('cropX', result.cropX);
        sessionStorage.setItem('cropY', result.cropY);
        sessionStorage.setItem('cropW', result.cropW);
        sessionStorage.setItem('cropH', result.cropH);
      }
    });
    $('.jwc_frame').addClass('center-block');
  }

  var upload_button = new ButtonUpload({
  	input: $("#photo_input")[0],
  	button: $("#photo_button")[0],
  	photoPlaceholder: $("#photo_placeholder"),
  	photoOptions: {
  		class: "crop_me center-block",
  		style: "",
  		id: "selected_photo"
  	},
  	afterClick: function(input, button) {},
  	onChange: function() {
  		console.log("Photo Changed!");
      init_cropper();
  	}
  });
}());
