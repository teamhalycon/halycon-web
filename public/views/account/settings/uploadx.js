
var crop_me = {};

var window_crop_init = function(id) {
  $(id).jWindowCrop({
    targetWidth: 256,
    targetHeight: 256,
    loadingText: ' ',
    onChange: function(result) {
      crop_me.cropX = result.cropX;
      crop_me.cropY = result.cropY;
      crop_me.cropW = result.cropW;
      crop_me.cropH = result.cropH;
    }
  });
};

$('#from_disk_input').change(function(e) {
  var file = e.originalEvent.srcElement.files[0];
  var img = document.createElement("img");
  img.id = 'img_preview';
  var reader = new FileReader();
  reader.onloadend = function() {
    img.src = reader.result;
  };
  reader.readAsDataURL(file);
  $('#photo_preview_disk').html(img);
  window_crop_init('#img_preview');
});

$('#upload_disk_photo').click(function() {
  var l = Ladda.create($('#upload_disk_photo')[0]);
  var form_data = new FormData($('#from_disk_form')[0]);
  form_data.append('crop_me', JSON.stringify(crop_me));
  form_data.append('type', 'disk');
  var form = new FormUpload({
    url: '/account/settings/profile_photo/',
    data: form_data,
    beforeSendHandler: function() { l.start(); },
    completeHandler: function(data) {
      l.stop();
      console.log(data);
      if (data.message == "ok")
        $('#user_photo').attr('src', data.photo);
    },
    errorHandler: function() {},
    progressHandler: function(e) {
      if (e.lengthComputable) {
        var percentComplete = e.loaded / e.total;
        l.setProgress(percentComplete);
      }
    }
  });
  form.upload();

});


$('#upload_url_photo').click(function() { });

$('#upload_camera_photo').click(function() {
  var l = Ladda.create($('#upload_camera_photo')[0]);
  var photo_url = $('#canvas')[0].toDataURL();

  var form_data = new FormData();
  //form_data.append('profile_photo', file);
  form_data.append('crop_me', JSON.stringify(crop_me));
  form_data.append('canvas_data', photo_url);
  form_data.append('type', 'camera');
  console.log(crop_me);
  var form = new FormUpload({
    url: '/account/settings/profile_photo/',
    data: form_data,
    beforeSendHandler: function() {
      l.start();
    },
    completeHandler: function(data) {
      l.stop();
      if (data.message == "ok")
        $('#user_photo').attr('src', data.photo);
    },
    errorHandler: function() {},
    progressHandler: function(e) {
      if(e.lengthComputable)
      {
        var percentComplete = e.loaded / e.total;
        l.setProgress(percentComplete);
        console.log(percentComplete);
      }
    },
  });
  form.upload();
});

var url_upload = false;
var url = '';
$('#fetch_url_photo').click(function() {
  var l = Ladda.create($('#fetch_url_photo')[0]);
  l.start();
  url = $('#profile_photo_url').val();
  var img = $('#photo_preview_url');
  var image = new Image();
  image.src = url;
  image.id = "fetched_photo";

  image.onload = function() {
    img.empty().append(image);
    window_crop_init(image);
    $('#upload_url_photo').removeAttr('disabled');
    url_upload = true;
    l.stop();
  };
  
  image.onerror = function() {
    img.empty().html("<h2 class='center-block' style='margin-top: 105px;'>Image not available</h2>");
    $('#upload_url_photo').attr('disabled', 'disabled');
    url_upload = false;
    l.stop();
  };

  img.empty().html("Loading...");

});


$('#upload_url_photo').click(function() {
  // TODO: upload
  if (url_upload) {
    var l = Ladda.create(this);
    var form_data = new FormData();
    form_data.append('type', 'url');
    form_data.append('url', url);
    form_data.append('crop_me', JSON.stringify(crop_me));
    var form = new FormUpload({
      url: '/account/settings/profile_photo/',
      data: form_data,
      beforeSendHandler: function() {
        l.start();
      },
      completeHandler: function(data) {
        l.stop();
        if (data.message == "ok")
          $('#user_photo').attr('src', data.photo);
      },
      errorHandler: function() {},
      progressHandler: function(e) {
        if(e.lengthComputable)
        {
          var percentComplete = e.loaded / e.total;
          l.setProgress(percentComplete);
        }
      }
    });
    form.upload();
  }
});


$(document).ready(function() { 
  $('#user_photo_btn').click();
  $('#photo_camera_btn').click();
  
  window_crop_init('.crop_me');
});