(function() {
	'use strict';

	var app = angular.module('app', []);

	//if ($('#qs_input')) {
	    app.controller('qs_cntrl', function($rootScope, $http) {
	        $rootScope.quickstream = function() {
	            window.location = '/' + $('#qs_input').val().replace(/\s+/g, "-").toLowerCase();
	        }
	    });
    //}

    app.controller('main', function($rootScope, $http) {
    	$rootScope.channel_selected = {};
        $rootScope.profile_videos = [];
        $rootScope.following = JSON.parse($('#following').val());

    	$rootScope.username = $('#profile_username').html();

        $rootScope.follow = function() {
            $http.get('/profile/follow/' + $rootScope.username)
                .success(function(data, status, headers, config) {
                    console.log(data);
                    if (data.message == "ok") {
                        $rootScope.following = true;
                        $('#followers').html(Number($('#followers').html()) + 1);
                    }
                });
        };

        $rootScope.unfollow = function() {
            $http.get('/profile/unfollow/' + $rootScope.username)
                .success(function(data, status, headers, config) {
                    console.log(data);
                    if (data.message == "ok") {
                        $rootScope.following = false;
                        $('#followers').html(Number($('#followers').html()) - 1);
                    }
                });
        };

        //console.log('Main: Fetching videos ...');
    	$http.get('/profile/get-videos/' + $rootScope.username)
    		.success(function(data, status, headers, config) {
    			console.log(data);
    			if (data.message == "ok") {
    				$rootScope.profile_videos = data.data;
    			}
    		});
    	$http.get('/profile/get-events/' + $rootScope.username)
    		.success(function(data, status, headers, config) {
    			console.log(data.data);
    			if (data.message == "ok") {
    				$rootScope.profile_events = data.data;
    				for (var i in $rootScope.profile_events) {
    					$rootScope.profile_events[i].data.start_datetime = moment($rootScope.profile_events[i].data.start_datetime).calendar();
                        console.log($rootScope.profile_events[i]);
                        $rootScope.profile_events[i].meta.photo_path = $rootScope.profile_events[i].meta.photo_thumbnail_path;
    				}
    			}
    		});
        $http.get('/profile/get-channels/' + $rootScope.username)
            .success(function(data, status, headers, config) {
                if (data.message == "ok") {
                    $rootScope.channels = data.data;
                    for (var i in $rootScope.channels)
                        if ($rootScope.channels[i].meta.default_channel == true)
                            $rootScope.channel_selected = $rootScope.channels[i];
                    if ($rootScope.channel_selected.meta.status == "Active") {
                        console.log("Channel is active");
                        $('#livestream').html("<iframe src='/public/"
                            + $rootScope.channel_selected.data.name + "' height='515' width='830' name='"
                            + $rootScope.channel_selected.data.name + "' scrolling=no align='center' class='center-block' frameborder='0' marginheight=0 marginwidth=0 allowFullScreen></iframe>");
                    }
                }
            });
    });

    var show_tab = function(tabname) {
        $('#' + tabname + '_btn').tab('show');
    }

    $(document).ready(function() {
    	console.log('Document ready');
    	show_tab('my_channels');
    })
}());
