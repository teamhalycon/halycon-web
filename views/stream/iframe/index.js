'use strict';

exports.iframe = function(req, res) {
	var Channel = req.app.db.models.Channel;
	Channel.findOne({ 'data.name': req.params.channelname}).exec(function(_err, _channel) {
		if (_err) { res.render('http/500'); }
		if (_channel != null) {
			res.render('stream/iframe/index', { channel: _channel });		
		}
	});
	
}