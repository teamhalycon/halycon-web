'use strict';

require('shelljs/global');
var async = require('async'),
    avconv = require('avconv'),
    fs = require('fs');


exports.stream = function(req, res) {

  var authenticated_resp = function() {
    var templ_dict = { user: req.user };

    var Channel = req.app.db.models.Channel;
    var Star = req.app.db.models.Star;

    Channel.findOne({ 'data.name': req.params.name }).exec(function(err, _channel) {
      if (err) {
        res.render('http/500');
      }
      if (_channel != null) {
        templ_dict.channel = _channel; 

        if (req.user.username === _channel.channelCreated.name) 
          templ_dict.channel.meta.status = "Active";
        
        if (templ_dict.channel.meta.status != "Active")
          res.redirect('/profile/' + _channel.channelCreated.name);

        // TODO: Get IP address
        templ_dict.channel.stats.total_views = templ_dict.channel.stats.total_views + 1;
        templ_dict.channel.save(function() {
          Star.findOne({ 'data.channel': _channel, 'starCreated.id': req.user }).exec(function(err, _star) {
            if (_star != null) 
              templ_dict.starred = true;
            else 
              templ_dict.starred = false;
            console.log('Channel updated ', templ_dict.channel.stats.total_views);
            console.log('templ_dict.starred ', templ_dict.starred);
            res.render('stream/index', templ_dict);           
          });
        });
      } else {
        // TODO: Redirect on create / buy channel

      }

    });
  }

  if (!req.isAuthenticated()) {
    res.redirect('/login/');
    // TODO: redirect res.redirect('/profile/' + req.params.name);
    // INFO: redirect to profile user + channel where he will connect as a viewer to room / stream if stream is active
    
  } else {
    authenticated_resp();
  }
}


// REST API

exports.star_this = function(req, res) {
  // +1
  var Channel = req.app.db.models.Channel;
  var Star = req.app.db.models.Star;

  Channel.findOne({ 'data.name': req.params.name }).exec(function(err, _channel) {
    if (err) { res.json({ message: "err" }); }
    if (_channel != null) {
      _channel.stats.stars = _channel.stats.stars + 1;
      
      var new_star = function() {
        var star = new Star({
          data: {
            channel: _channel
          },
          starCreated: {
            id: req.user,
            name: req.user.username,
          }
        });

        star.save(function() {
          _channel.save(function() {
            req.user.stars = req.user.stars + 1;
            req.user.save(function() {
              res.json({
                message: "ok",
                stars: _channel.stats.stars
              });  
            });
              
          });
        });
      }

      Star.count({ 'data.channel': _channel, 'starCreated.id': req.user }, function(err, _count) {
        if (err) { res.json({ message: "err" }); }
        if (_count > 0) {
          res.json({ 
            message: "exists",
            stars: _channel.stats.stars
           });
        } else {
          new_star();
        }
      });
      
    }
  });
}

exports.unstar_this = function(req, res) {
  // -1
  var Channel = req.app.db.models.Channel;
  var Star = req.app.db.models.Star;

  var finish = function(channel, star) {
    channel.save(function() {
      star.remove(function() {
        req.user.stars = req.user.stars - 1;
        req.user.save(function() {
          res.json({
            message: "ok",
            stars: channel.stats.stars
          });  
        });
        
      })
    });
  }

  Channel.findOne({ 'data.name': req.params.name }).exec(function(err, _channel) {
    if (err) { res.json({ message: "err" }); }
    if (_channel != null) {
      _channel.stats.stars = _channel.stats.stars - 1;
      
      Star.findOne({ 'data.channel': _channel, 'starCreated.id': req.user }, function(err, _star) {
        if (err) 
          res.json({ message: "err" });
        if (_star)
          finish(_channel, _star);
        console.log('Unstarring', err, _star);
      });
    }
  });
}

exports.send_report = function(req, res) {
  var report = req.body.report;
  if (report == undefined) 
    res.json({ message: "err" });

  var channel = req.params.name;
  var Channel = req.app.db.models.Channel;
  console.log('Sending report', report);

  Channel.findOne({ 'data.name': req.params.name }).exec(function(err, _channel) {
    if (err) { res.json({ message: "err" }); }
    if (_channel != null) {
      req.app.utility.sendmail(req, res, {
        from: req.user.username +' <'+ req.user.email +'>',
        to: "report@halycon.com",
        subject: "Report for stream: " + req.params.name,
        textPath: 'stream/report/email-text',
        htmlPath: 'stream/report/email-html',
        locals: {
          username: req.user.username,
          email: req.user.email,
          stream_name: req.params.name,
          report_title: report.title,
          report_description: report.description,
          project_name: "Halycon.TV"
        },
        success: function(message) {
          res.json({ message: "ok" });
        },
        error: function(err) {
          console.log("Error while sending report", err);
          res.json({ message: "err" });
        }
      });
    }
  });
};

exports.users_online = function(req, res) {
  // GET how many users are online - refresh
  var packet = req.body.packet;
  var Channel = req.app.db.models.Channel;

  console.log(packet);
  Channel.findOne({ 'data.name': packet.channel }).exec(function(err, _channel) {
    console.log("Wet willy", err, _channel);
    if (err) { res.json({ message: "err" }); }
    if (_channel != null) {
      console.log("Checking: ", _channel.stats.online_viewers.max, packet.users_online);
      if (_channel.stats.online_viewers.max < packet.users_online) {
        _channel.stats.online_viewers.max = packet.users_online;
        console.log("Updated: ", packet.users_online)
        _channel.save(function() {
          res.json({ message: "ok" });
        });
      } else {
        res.json({ message: "null" });
      }
    }
  });
};


exports.new_share = function(req, res) {
  // +1 on shares for each social network share
};


exports.channel_deactivate = function(req, res) {
  var Channel = req.app.db.models.Channel;
  Channel.findOne({ 'data.name': req.params.name }).exec(function(err, _channel) {
    if (err) { res.json({ message: "err" }); }
    if (_channel != null) {
      _channel.meta.status = "Inactive";
      _channel.save(function() {
        res.json({ message: "ok" });
      });
    }
    else {
      res.json({ message: "err" });
    }
  });
};

exports.stop_recording = function(req, res) {

  // avconv -i recording118746693711727860.mkv -c:v copy -qscale:a 6 -c:a libvorbis -f webm output.webm

  var Channel = req.app.db.models.Channel;
  var Video = req.app.db.models.Video;

  var video_path = req.body.video_path;
  var channel_name = req.body.channel;
  var copy_to = './public/media/channels/' + channel_name;
  
  if (!fs.existsSync(copy_to))
    fs.mkdirSync(copy_to);
  
  var nvideo_name = 'video' + Date.now() + '.webm'
  var nvideo_path = copy_to + '/' + nvideo_name;
  var nvideo_url = '/media/channels/' + channel_name + '/' + nvideo_name;

  var avconv_params = [
    '-i', video_path,
    '-c:v', 'copy',
    '-c:v', 'libvpx',
    //'-qscale:a', '6',
    //'-c:a', 'libvorbis',
    '-f', 'webm', nvideo_path
  ];

  var resp = function(err, _data) {
    if (err) { res.json({ message: err }); }
    else {
      res.json({ message: "ok", data: _data });
    }
  }

  var video_convert = function() {
    var stream = avconv(avconv_params);
    stream.on('error', function(data) {
      resp(true, data);
    });
    stream.once('end', function(exitCode, signal) {
      resp(false, null);
    });
  }

  Channel.findOne({ 'data.name': channel_name }).exec(function(_err, _channel) {
    if (_err) { resp(true, null); }
    if (_channel != null) {
      var new_video = new Video({
        data: {
          channel: _channel,
          path: nvideo_url
        },
        videoCreated: {
          id: req.user,
          name: req.user.username
        }
      });
      new_video.save(function() {
        video_convert();
      });
    }
  });
  
};