'use strict';

exports.init = function(req, res){
	if (req.isAuthenticated())
		res.redirect('/login/');
	else
  		res.render('index', { title: "Halycon.TV" });
};
