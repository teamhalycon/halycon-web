'use strict';

exports.api = function(req, res) {
	var languages = ['English', 'Croatian'];
	res.json(languages);
}