'use strict';

exports.init = function(req, res) {
	res.render('events/index', { user: req.user });
};

exports.my_events = function(req, res) {
	req.app.db.models.Event.find({ 'eventCreated.id': req.user }).exec(function(err, events) {
		if (err) {
			res.json({ message: 'err' });
		} else {
			res.json(events);
		}
	});
};