'use strict';

exports.init = function(req, res) {
	console.log(req.body);
	var dict = { user: req.user, new_event: req.body.event.name };
	// TODO: Capitalize and validate name
	res.render('events/new_event/index', dict);
};


var get_photo_names = function(str) {
	// Args: str - photo filename
	// Returns list with photo names
	// eg. get_photo_name('photo.gif') -> ['photo.gif', 'photo_thumb.gif']
    var str_arr = str.split(".");
    var ext = "." + str_arr.pop();
    var sol = [ str_arr.join() + ext, str_arr.join() + "_thumb" + ext];
    return sol;
}

exports.new_event = function(req, res) {

	var fs = require('fs'),
		gm = require('gm'),
		events_path = 'public/media/events/';

	var Event = req.app.db.models.Event;

	// TODO: validation
	
	var channel_doc = JSON.parse(req.body.channel);
	if (channel_doc) 
		delete channel_doc['$$hashKey'];
	var folder_name_stripped = req.body.name.replace(/ /g,'');
	//console.log(channel_doc);

	var new_event = new Event({
		data: {
			name: req.body.name,
			start_datetime: JSON.parse(req.body.start_datetime),
			end_datetime: JSON.parse(req.body.end_datetime),
			description: req.body.description,
		}, 
		channel: {
			id: channel_doc._id,
			name: channel_doc.data.name
		},
		meta: {
			folder_hash: folder_name_stripped + "_" + new Date().getTime(),
			photo_path: (req.files.promo_photo.size != 0) ? req.files.promo_photo.path : '',
		},
		eventCreated: {
			id: req.user,
			name: req.user.username
		}
	});

	console.log(new_event);
	//console.log('Creating event folder');
	// create directory for new event
	fs.mkdirSync(events_path + new_event.meta.folder_hash);
	//console.log('Event folder created');

	if (req.files.promo_photo.size != 0 && new_event.meta.photo_path != '') {
		//console.log('File is uploaded, saving file.');
		fs.readFile(req.files.promo_photo.path, function(err, file_loaded) {
			var copy_to = events_path + new_event.meta.folder_hash + '/' + get_photo_names(req.files.promo_photo.name)[0];
			//console.log('File has been read from /tmp folder, trying to copy to ' + copy_to);
			fs.writeFile('./' + copy_to, file_loaded, function(err) {
				if (err) { res.json({ message: 'err' }); }
				var resize_data = JSON.parse(req.body.cropper);

				var dest_path = events_path + new_event.meta.folder_hash + '/' + get_photo_names(req.files.promo_photo.name)[1];
				//console.log("Creating thumbnail of the photo, to " + dest_path);
				//console.log("Resize data " + JSON.stringify(resize_data) );

				// REVIEW: Potencial bug here!
				gm(copy_to)
					.crop(resize_data.cropW, resize_data.cropH, resize_data.cropX, resize_data.cropY)
					.write(dest_path, function(err) {
					
						if (err) { res.json({ message: 'err' }); }
						else {
							new_event.meta.photo_path = '/' + encodeURI( copy_to.substring(7) );
							new_event.meta.photo_thumbnail_path = '/' + encodeURI( dest_path.substring(7) );
							new_event.save();
							res.json(new_event);
						}
					
					});
				
			});
		});
	} else {
		new_event.save();
		res.json(new_event);
	}

	// TODO: invitations
	// TODO: invoice payment
	// TODO: donations
	// TODO: create new event

	/*
	console.log(req.body);
	console.log(req.files);
	setTimeout(function() {
		res.json({ message: 'ok' });	
	}, 5000);
	*/
}
