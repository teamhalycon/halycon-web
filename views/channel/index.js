'use strict';

require('shelljs/global');
var fs = require('fs'),
    gm = require('gm');


exports.channels = function(req, res) {
  req.app.db.models.Channel.find({ 'channelCreated.id': req.user }).exec(function(err, channels_collection) {
    if (err) {
      res.json({ err: true });
    } else {
      res.json(channels_collection);
    }
  });
}


exports.new_channel = function(req, res) {
  
  // TODO: check if paid

  var Channel = req.app.db.models.Channel;
  var packet = req.body;

  var onsave_callback = function(err, channel) {
    if (err) {
      res.json({ err: true});
    }
    res.json(channel);
  }

  console.log(packet);
  // TODO: Validation

  if (packet._id) {
    Channel.find({ _id: packet._id }).exec(function(err, channel) {
      if (err) {
        res.json({ err: true });
      }
      if (packet.data)
        channel.data = packet.data;
      if (packet.meta)
        channel.meta = packet.meta;
      channel.save(onsave_callback);
    });
  } else {
    var channel = new Channel({
      data: packet.data,
      channelCreated: {
        id: req.user,
        name: req.user.username
      }
    });
    channel.save(onsave_callback);
  }
};

exports.edit_channel = function(req, res) {
  /*
    COMMENT: req.body example 
       { id: '52e94ee479bc0cae7230387f',
      name: 'sam',
      type: 'Public',
      status: 'Active',
      languages: '',
      categories: 
       [ 'Fun',
         'Education',
         'Computer Science',
         'Development',
         'Security' ],
      description: '' }

   */

  var resp = function(_err, _data) {
    res.json({ message: _err, data: _data });
  }

  console.log(req.body);
  var Channel = req.app.db.models.Channel;
  Channel.findOne({ '_id': req.body.id, 'channelCreated.id': req.user }).exec(function(_err, _channel) {
    if (_err) { resp("err", null); }
    if (_channel != null) {
      // TODO: VALIDATE
      _channel.data.name = req.body.name;
      _channel.data.type = req.body.type;
      _channel.meta.status = req.body.status;
      _channel.meta.language = req.body.language;
      _channel.meta.categories = req.body.categories;
      _channel.meta.description = req.body.description;
      _channel.save(function() {
        resp("ok", _channel);
      });
    }
  });
}

exports.edit_channel_photo = function(req, res) {
  var Channel = req.app.db.models.Channel;
  var resize_data = JSON.parse(req.body.cropper);
  var photo_path = 'public/media/channels/';
  var username = req.user.username;
  var channel_name = req.body.channel;
  var write_stream = photo_path + username;

  if (!fs.existsSync(write_stream))
      fs.mkdirSync(write_stream);

  var resp = function(_err, _data) {
    res.json({ message: _err, data: _data });
  }

  var update_channel = function() {
    Channel.findOne({ 'data.name': channel_name }).exec(function(_err, _channel) {
        if (_err) { resp("err", null); };
        if (_channel != null) {
          _channel.data.photo = '/media/channels/' + username + '/thumb_' + req.files.channel_photo.name;
          _channel.save(function() {
            resp("ok", { new_path: _channel.data.photo });
          });
        } else {
          resp(null, null);
        }
    });
  }

  var fs_handler = function(from, to) {
    gm(from)
      .crop(resize_data.cropW, resize_data.cropH, resize_data.cropX, resize_data.cropY)
      .write(to, function(err) {
        if (err) {
          console.log("Error in fs_handler: ", err);
          //res.json({ message: 'err' });
          resp("err", null);
        }
        else {
          update_channel();
        }    
      });
  }

  if (req.files.channel_photo.size != 0) {
    //console.log("FILE: ", req.files);
    var copy_from = req.files.channel_photo.path;
    var copy_to = './' + write_stream + '/thumb_' + req.files.channel_photo.name;
    fs_handler(copy_from, copy_to);
  }
};


exports.public_channels = function(req, res) {
  var Channel = req.app.db.models.Channel;

  Channel.find({ 'data.type': 'Public' }).exec(function(err, channels) {
    if (err) {
      res.json({ message: 'err' });
    }
    res.json(channels);
  });
};