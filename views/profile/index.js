'use strict';


exports.init = function(req, res) {
  res.redirect('/account/');
};

exports.get_profile = function(req, res) {
  
  var User = req.app.db.models.User;
  var Follow = req.app.db.models.Follow;

  var render = function(resp_dict) {
    res.render('profile/index', resp_dict);
  };

  var get_follow = function(resp_dict) {
    Follow.findOne({ 'data.from': resp_dict.user, 'data.to': resp_dict.profile_user }).exec(function(_err, _follow) {
      if (_err) { 
        console.log(_err);
        res.render('http/500'); 
      }
      if (_follow != null) {
        resp_dict.following = true;
      } 
      render(resp_dict);
    });
  }

  User.findOne({ 'username': req.params.name }).populate('roles.account').exec(function(err, user) {
    if (err) {
      console.log(err);
      res.render('http/500');
    }
    if (user != null) {
      
      var date = user.timeCreated.toLocaleDateString().split(',');
      var resp_dict = { 
        user: req.user,
        profile_user: user, 
        joinDate: date[1] + ', ' + date[2],
        header: req.isAuthenticated(),
      };  

      resp_dict.following = false;
      resp_dict.show_follow = false;
      if (req.isAuthenticated())
        if (req.user.username != user.username)
          resp_dict.show_follow = true;

      if (resp_dict.show_follow)
        get_follow(resp_dict);
      else
        render(resp_dict);

    } else {
      console.log('No such user exists');
      res.render('http/404');
    } 
  });

};


// API for profile

exports.get_videos = function(req, res) {
  var Video = req.app.db.models.Video;
  var username = req.params.name;

  var resp = function(_err, _data) {
    res.json({ message: _err, data: _data });
  }

  Video.find({ 'videoCreated.name': username }).exec(function(_err, _videos) {
    if (_err) { resp("err", null); }
    if (_videos != null) {
      var response_data = [];
      
      for (var i in _videos) {
        if (_videos[i].data.type == "Public") {
          response_data.push(_videos[i].data);
        }
      } 
      resp("ok", response_data);
    }
  });
};

exports.get_events = function(req, res) {
  var Event = req.app.db.models.Event;
  var username = req.params.name;

  var resp = function(_err, _data) {
    res.json({ message: _err, data: _data });
  }

  Event.find({ 'eventCreated.name': username }).populate('channel.id').exec(function(_err, _events) {
    if (_err) { resp("err", null); }
    if (_events != null) {
      // TODO: check for public events
      /*for (var i in _events) {
        _events.data.datetime = moment().format('MMMM Do YYYY, h:mm:ss a');
      }*/
      console.log(_events);
      resp("ok", _events);
    }
  });
};

exports.get_channels = function(req, res) {
  var Channel = req.app.db.models.Channel;
  var username = req.params.name;

  var resp = function(_err, _data) {
    res.json({ message: _err, data: _data });
  };

  Channel.find({ 'channelCreated.name': username, 'data.type': 'Public' }).exec(function(_err, _channels) {
    if (_err) { resp("err", null); }
    if (_channels != null) {
      // TODO: check for public channels
      resp("ok", _channels);
    }
  });
};

exports.follow = function(req, res) {
  var username = req.params.name;
  var User = req.app.db.models.User;
  var Follow = req.app.db.models.Follow;

  var resp = function(_err, _data) {
    res.json({ message: _err, data: _data });
  }

  User.findOne({ 'username': username }).exec(function(_err, _user) {
    if (_err) { resp("err", null); }
    if (_user != null) {
      Follow.findOne({ 'data.from': req.user, 'data.to': _user }).exec(function(_ferr, _follow) {
        if (_ferr) { resp("err", null); }
        if (_follow != null) {
          resp("null", null);
        } else {
          
          var new_connection = new Follow({
            data: { 
              from: req.user,
              to: _user
            },
            followCreated: {
              id: req.user,
              name: req.user.username
            }
          });

          new_connection.save(function(_follow_err, _follow_doc) {
            req.user.following = req.user.following + 1;
            req.user.save(function(_user_err, _user_doc) {
              _user.followers = _user.followers + 1;
              _user.save(function() {
                resp("ok", _follow_doc);  
              });
            });
          });
          
        }
      });
    } else {
      resp("err", "user is null");
    }
  });
};

exports.unfollow = function(req, res) {
  var username = req.params.name;
  var User = req.app.db.models.User;
  var Follow = req.app.db.models.Follow;

  var resp = function(_err, _data) {
    res.json({ message: _err, data: _data });
  };

  User.findOne({ 'username': username }).exec(function(_err, _user) {
    if (_err) { resp("err", null); }
    if (_user != null) {
      Follow.findOne({ 'data.from': req.user, 'data.to': _user }).exec(function(_ferr, _follow) {
        if (_ferr) { resp("err", null); }
        if (_follow != null) {
          _follow.remove(function() { 
            _user.followers = _user.followers - 1;
            _user.save(function() {
              req.user.following = req.user.following - 1;
              req.user.save(function() {
                resp("ok", null);     
              });
            });
          });
        } else {
          resp("err", null);
        }
      });
    } else {
      resp("err", "user is null");
    }
  });
};