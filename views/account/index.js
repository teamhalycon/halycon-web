'use strict';

var async = require('async');

exports.init = function(req, res) {
  //console.log(Object.keys(req.app.db.models));
  req.app.db.models.Channel.find({ 'channelCreated.id': req.user }).exec(function(err, channels_collection) {
    console.log(channels_collection);
    if (err) {
      // TODO:
    }
    var render_resp = function() {
      var filter = {
        user: req.user,
        channels: channels_collection,
      };
      res.render('account/index', filter);
    }
    render_resp();
  });

};


// API
exports.my_stars = function(req, res) {

  var resp = function(err, data) {
    if (err)
      res.json({ message: "err" });
    else
      res.json({ message: "ok", data: data });
  }

  req.app.db.models.Star.find({ 'starCreated.id': req.user }, 'data.channel')
    .populate('data.channel').exec(function(_err, _stars) {
      if (_err)
        resp(true, null);
      if (_stars != null) {
        var resp_ = [];
        for (var i in _stars) {
          var channel_name = _stars[i].data.channel.channelCreated.name;
          resp_.push({ name: channel_name, url: "/profile/" + channel_name });
        }
        resp(false, resp_);
      }
    });
};

exports.news_feed = function(req, res) {
  // TODO: New events
  // TODO: New channels
  // TODO: New follow on user
  // TODO: Ticket bought
  // TODO: Channel I follow is active
  
  var news = Object({
    photo: "",
    name: "",
    description: "",
    profile_url: "",
    type: "",
  });
  var channels_resp = [];
  var events_resp = [];

  var resp_dict = {};
  var Event = req.app.db.models.Event;

  var resp = function(_msg, _data) {
    res.json({ message: _msg, data: _data });
  }

  var get_events = function() {
    var dt = Date.now();
    Event.find({ 'data.type': 'Public', 'data.start_datetime': { $gte: dt } }).exec(function(_err, _events) {
      if (_err) {
        resp_dict.events = null;
        console.log(_err);
      }
      if (_events.length > 0) {
        for (var index in _events) {
          var event = {
            photo: _events[index].meta.photo_thumbnail_path,
            name: _events[index].data.name,
            description: _events[index].data.description,
            stats: _events[index].stats,
            profile_url: '/profile/' + _events[index].eventCreated.name,
            type: "event"
          };
          events_resp.push(event);
        }
        resp_dict.events = events_resp;
      } else {
        resp_dict.events = null;
      }
      resp("ok", resp_dict);
    });
  };

  var get_channels = function() {
    var Channel = req.app.db.models.Channel;
    Channel.find({ 'data.type': 'Public', 'meta.status': 'Active'}).exec(function(_err, _channels) {
      if (_err) {
        resp_dict.channels = null;
      }

      if (_channels.length > 0) {
        for (var index in _channels) {
          var channel = {
            photo: _channels[index].data.photo,
            name: _channels[index].data.name,
            description: _channels[index].meta.description,
            stats: _channels[index].stats,
            profile_url: '/profile/' + _channels[index].channelCreated.name,
            type: "channel"
          };
          channels_resp.push(channel);
        }
        resp_dict.channels = channels_resp;

      } else {
        resp_dict.channels = null;
      }
      get_events();
    });
  };

  get_channels();

/*
  // TODO: get all upcomming events
  var get_upcoming = function() {
    
  };
*/

  //async.parallel(get_channels, get_upcoming, resp("ok", resp_dict));

};

exports.calendar_data = function(req, res) {
  // TODO: events scheduled
}
