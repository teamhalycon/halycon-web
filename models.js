'use strict';

exports = module.exports = function(app, mongoose) {

  // docs for app
  require('./schema/Channel')(app, mongoose);
  require('./schema/Event')(app, mongoose);
  require('./schema/Star')(app, mongoose);
  require('./schema/Video')(app, mongoose);
  
  require('./schema/Follow')(app, mongoose);
  
  //embeddable docs first
  require('./schema/Note')(app, mongoose);
  require('./schema/Status')(app, mongoose);
  require('./schema/StatusLog')(app, mongoose);
  require('./schema/Category')(app, mongoose);

  //then regular docs
  require('./schema/User')(app, mongoose);
  require('./schema/Admin')(app, mongoose);
  require('./schema/AdminGroup')(app, mongoose);
  require('./schema/Account')(app, mongoose);
  
};
